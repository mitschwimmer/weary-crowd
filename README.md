# Weary Crowd
An experimental app for evaluating the following in combination:

- Different Tech stack
- Streamed Web Application (via Websocket)

## Goals
This experiment is successful, if I can get
- a nice component approach for managing web client UI from the backend
- good separation between several clients

## Tech
- Java (Kotlin would be too easy)
- Undertow via the Jakarta APIs Websocket JSR 356 and Servlet 5.0
- Maven instead of Intellij Build Tool (or Gradle..)
- Velocity as Template Engine (Maven is using this for archetypes so I am inclined to try it)

## Experiments
- [X] Use Jakarta with minimal dependencies. Basically just Undertow.
- [X] Serve a welcome page via a servlet (as "addWelcomPage()" is not finding HTML files on the classpath)
- [X] Use Websocket JSR 356 to establish a connection to a client browser.
- [ ] Manage separate client states on the backend
- [ ] Use Velocity for a part of the UI
- [ ] Have a text input, where each input is sent to the server. Server than sends back an updated component showing the input.
- [ ] Maybe a state machine in java? https://www.baeldung.com/java-enum-simple-state-machine
- [ ] Test in small JRE "eclipse-temurin:17-jre-alpine" 

## Decisions
### Why a template engine?
We could use MessageFormat or String.format(), but both work with placeholders that get referenced by their index alone.
This is not great as there might be quite a few of them on a page. The other downside is that to the IDE, the "template" is just a string. Syntax highlighting does work only in Intellij Ultimate then.