package de.mitschwimmer.websocketJSR;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

/**
 * @author Stuart Douglas
 */
public class MessageServlet extends HttpServlet {
    private String welcome;


    @Override
    public void init(final ServletConfig config) throws ServletException {
        super.init(config);
        try (var asStream = this.getClass().getResourceAsStream("/index.html")) {
            if (asStream == null) throw new NullPointerException();
            welcome = new String(asStream.readAllBytes(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new NullPointerException("Reading a file failed.");
        }
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        PrintWriter writer = resp.getWriter();
        writer.write(welcome);
        writer.close();
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        doGet(req, resp);
    }
}